#!/bin/bash
# setup_ether.sh
# Jun Go gojun077@gmail.com
# Last updated 2016-07-28

# Script to setup wired interface
# 1. Raise iface
# 2. Clear any existing IP on iface
# 3. Add IP to iface
# This script should be executed as root

# USAGE:
#sudo ./setup_ether.sh [iface-name] [IP]
# example: sudo ./setup_ether.sh ens1 192.168.95.101/24

if [ -z "$1" ]; then
  echo "Must enter the name of the wired iface"
  exit 1
elif [ -z "$2" ]; then
  echo "Must enter IP address"
  exit 1
elif [[ "$2" != */* ]]; then
  echo "Must enter subnet after '/' character"
  exit 1
else
  ip l set "$1" up
  ip a flush dev "$1"
  ip a add "$2" dev "$1"
fi
