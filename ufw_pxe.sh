#!/bin/bash
# ufw_pxe.sh
# Created by Jun Go gojun077@gmail.com
# Last Updated 2016-07-12

# This script will open ports needed for
# running a PXE server

# This script must be run as root

# This script has been tested on an Ubuntu 16.04
# server.

# USAGE
# sudo ./firewalld_pxe.sh <parameter>
# where <parameter> can be either 'open' or 'close'
# Parameter 'open' will cause ports or services to be
# added to the ufw whitelist, while 'close' will
# cause them to removed

if [ "$1" = "open" ]; then
  CMD=allow
elif [ "$1" = "close" ]; then
  CMD=deny
else
  echo -e "Parameter must be either 'open' or 'close'\n" 1>&2
  exit 1
fi


#################
#   TCP PORTS
#################
# darkhttpd
DARK1=8080
DARK2=8000
VNC=5500
# ssh (this is enabled by default; no need to add)
#SSH=22

TCPPORTS=($DARK1
          $DARK2
          $VNC
         )


#################
#   UDP PORTS
#################
#DHCPSERV=68
#DHCPCLIENT=67
#TFTP=69

#UDPPORTS=($DHCPSERV
#          $DHCPCLIENT
#          $TFTP
#         )


#################
#   SERVICES
#################
# On Ubuntu refer to /etc/services for a list of
# recognized services and their TCP/UDP ports
SERVICES=(bootpc #68/tcp/udp
          bootps #67/tcp/udp
          http   #80/tcp/udp
          tftp   #69/udp
         )

#################
#   MAIN PROG
#################

# ENABLE SERVICES REQ'D FOR PXE
for i in ${SERVICES[*]}; do
  ufw $CMD "$i"
done

for j in ${TCPPORTS[*]}; do
  ufw $CMD "$j"/tcp
done

# List open ports
ufw status
