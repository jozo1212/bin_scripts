#!/bin/bash
# bittorrent.sh
# Last updated 2016-08-15
# Jun Go gojun077@gmail.com

# This script should be launched as ROOT
# - Open TCP 51413 in firewalld
# - Wait 5 sec.
# - while 'pidof transmission-gtk' returns '0'
#   noop
# - when 'pidof transmission-gtk' returns '1'
#   close TCP 51413 in firewalld

# Edit the default zone setting to match that
# of your machine
DZONE=$(firewall-cmd --get-default)
firewall-cmd --zone="$DZONE" --add-port 51413/tcp
sleep 5
while pidof transmission-gtk > /dev/null; do
  :
done

firewall-cmd --zone="$DZONE" --remove-port 51413/tcp
