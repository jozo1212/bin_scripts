Snippet for ssh-add
===================

In Desktop Environments such as XFCE4, you can invoke

`ssh-agent startxfce4`

to start the graphical session. Once XFCE launches, a
terminal window will spawn, prompting you for the
password of your ssh key which you have registered
using `ssh-add` in a previous session.

If you launch an Openbox graphical session with

`ssh-agent openbox-session`

the _ssh-agent_ daemon will start but you must manually
add your ssh private key file to the ssh-agent with

`ssh-add $HOME/.ssh/archjun_rsa`

After which you will be prompted to enter your passphrase.
Once you have entered it, however, you won't be prompted
again, as ssh-agent will remember it and enter it for
you.
