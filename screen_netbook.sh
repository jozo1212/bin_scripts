#!/bin/bash

#screen_netbook.sh
#This script launches Screen with various tabs and starts various programs
#such as htop, cmus, etc. as well as custom scripts in separate tabs.
#It is intended to be used in tty-only sessions on netbooks that don't
#have enough memory and CPU to handle X11 graphical sessions

# Meaning of GNU Screen option flags:
# -A    adapt the sizes of all windows to the size of the current terminal
# -d -m launch GNU Screen in detached mode; creates a new session but
#       doesn't attach to it
# -S    When creating new session, specify session name
# -p    preselect a window (by no. or name)
# -X    send cmd to running screen session

# to list running Screen sessions, 'screen -ls'
# to reconnect to a Screen session, 'screen -r sessionName'

# launch a GNU Screen session named 'netbook' in detached mode
screen -AdmS netbook -t SHELL
# create separate tabs in session 'netbook' and launch programs in tabs
screen -S netbook -X screen -t HTOP htop
screen -S netbook -X screen -t DMESG dmesg -wH
screen -S netbook -X screen -t CMUS cmus
screen -S netbook -X screen -t IRSSI irssi
screen -S netbook -X screen -t EMACS emacs

# On tab 0, launch SpiderOakONE and dropboxd daemons for cli
screen -S netbook -p 0 -X stuff "cd $HOME/bin^M"
screen -S netbook -p 0 -X stuff "./tty_startup.sh^M"
