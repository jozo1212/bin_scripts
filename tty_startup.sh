#!/bin/bash

# This script launches various daemons for a non-graphical
# session, i.e. tty session
# dropbox-cli is an Archlinux AUR package of dropbox.py, the
# official commandline client for Dropbox. On non-Arch distro's
# get dropbox.py from the following link:
#http://www.dropboxwiki.com/tips-and-tricks/using-the-official-dropbox-command-line-interface-cli

SpiderOakONE --headless &
dropbox-cli start &
