wpa_supplicant snippets
=======================

For manually  connecting to wi-fi via wpa_supplicant

1. Create a config file to store AP info
The commands below assume that SELINUX is in enforcing mode
`cd /etc/wpa_supplicant/`
`sudo cp --preserve=context wpa_supplicant.conf local.conf`

2. Add `update_config=1` to `/etc/wpa_supplicant/local.conf`

3. Launch `wpa_supplicant` in daemon mode for wireless interface
`sudo wpa_supplicant -B -i wiface -c /etc/wpa_supplicant/local.conf`
where `-B` option launches `wpa_supplicant` as a background daemon

4. Execute `wpa_cli`
If you don't have any AP's saved in your config file you must
run 'wpa_cli' to scan for AP's and manually connect to one
`sudo wpa_cli`

5. Scan for AP's
`scan`

6. View list of scanned AP's
`scan_results`

7. Find next available network number
`add_network`

8. Assign a scanned AP to a network number (in this case '0')
`set_network 0 ssid "nameOfAP"`

9. Enter password for network *n*
`set_network 0 psk "password"`

10. Enable connection to network *n*
`enable_network 0`

11. Save network in the config file
Note that this requires `update_config=1` to exist in the conf file
`save_config`

12. Quit wpa_cli
`quit`

13 Get IP from AP through DHCP
`sudo dhclient wiface`
