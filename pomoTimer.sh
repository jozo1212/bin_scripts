#!/bin/bash
# pomoTimer.sh
# Jun Go gojun077@gmail.com
# Last Updated 2016-06-10

# Script to be executed by pystopwatch or xfce4-timer-plugin.
# It plays a sound using mplayer and then calls pomo2beeminder.sh
# to send number of pomodoros completed to Beeminder for goal
# gojun077/pomodoro

# USAGE:
# ./pomoTimer.sh <number_of_pomodoros>

SOUND="$HOME/SpiderOak Hive/sound/zen_temple_bell-soundbible-com.wav"
POMPATH="$HOME/Documents/pomo2beeminder"

if [ -z "$1" ]; then
  echo -e "You must specify the number of pomodoros\n"
  exit 1
else
  mplayer "$SOUND"
  "$POMPATH"/pomo2beeminder.sh "$1"
fi
