#!/bin/bash

# This script is intended to be run by anacron or cron(ie)
# to backup the Mnemosyne sqlite3 database 'default.db' to
# SpiderOak. For this script to work, a symlink from the
# '~/SpiderOak Hive' folder to '~/SpiderOak_Hive' must exist

MNEMOPATH=/home/archjun/Dropbox/mnemosyne
SPDRPATH=/home/archjun/SpiderOak_Hive
TIMESTMP=$(date +%F_%H.%M)   #yyyy-mm-dd_HH.MM format

cp $MNEMOPATH/default.db "$SPDRPATH/mnemosyne_bkup/$TIMESTMP.db"
