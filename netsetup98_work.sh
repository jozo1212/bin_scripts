#!/bin/bash
# Script to setup wired interface on ASUS U36JC notebook
# Also adds a route to a server connected to the external
# network
# This script should be executed as root

ip l set ens5 up
ip a flush dev ens5
ip a add 192.168.95.98/24 broadcast 192.168.95.255 dev ens5
# Add default route pointing to server with dual NIC's connected to
# both the internal and external networks
ip r add default via 192.168.95.145
# Add nameserver if none specified
if grep -w nameserver /etc/resolv.conf; then
  echo "nameserver already exists"
  exit 0
else
  echo "nameserver 192.168.30.1" >> /etc/resolv.conf
fi
