#!/bin/bash
# setup-bridge.sh
# Last Updated: 2016-10-7
# Jun Go gojun077@gmail.com

# This script creates a linux bridge with a single ethernet
# iface as a slave. It takes 2 arguments:
# (1) Name of bridge
# (2) wired interface name (to be slave of bridge)
# The ip address for the bridge will be assigned by the script
# 'setup_ether.sh'

# This script must be executed as root and requires the
# packages bridge-utils and iproute2 to be installed.

# USAGE sudo ./setup-bridge <bridgeName> <wiredIfaceName>
# (ex) sudo ./setup-bridge br0 enp1s0


brname=$1
ethname=$2

if [ -z "$brname" ]; then
  echo "Must enter bridge name"
  exit 1
elif [ -z "$ethname" ]; then
  echo "Must enter name of wired iface to be used as slave for bridge"
  exit 1
else
  # create bridge and change its state to UP
  ip l add name "$brname" type bridge
  ip l set "$brname" up
  # set bridge forwarding delay to 0
  brctl setfd "$brname" 0
  # Add wired interface to the bridge
  ip l set "$ethname" up
  ip l set "$ethname" master "$brname"
  # Show active interfaces
  ip a show up
fi
