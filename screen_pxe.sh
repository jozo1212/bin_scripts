#!/bin/bash

# screen_pxe.sh
# This script launches Screen with various tabs and starts programs
# including dnsmasq, darkhttpd, vncviewer, and bash.

# Meaning of GNU Screen option flags:
# -A    adapt the sizes of all windows to the size of the current terminal
# -d -m launch GNU Screen in detached mode; creates a new session but
#       doesn't attach to it
# -S    When creating new session, specify session name
# -p    preselect a window (by no. or name)
# -X    send cmd to running screen session

# to list running Screen sessions, 'screen -ls'
# to reconnect to a Screen session, 'screen -r sessionName'

# USAGE
# ./screen_pxe.sh -e [wired iface] -i [path-to-iso] -k [kickstart-path] -v
# -e : wiredIface@IPaddr i.e. ens1@192.168.95.97/24
# -f : firewall type 'firewalld' or 'ufw'
# -i : path to iso image to mount on /mnt/distroIso and serve over http
#      NOTE: the mount point '/mnt/distroIso' must exist!
# -k : /path/to/ks/directory
# -v : launch vncviewer in listen mode (for vnc reverse connect)


# Before launching GNU Screen PXE environment, check if NetworkManager
# is running and stop the NM systemd service if it is.
if systemctl --quiet is-active NetworkManager; then
  systemctl stop NetworkManager
fi

# launch a GNU Screen session named 'pxeserver' in detached mode
# and create bash shell tabs

screen -AdmS pxeserver        -t sh0     # tab 0 'sh'
screen -S pxeserver -X screen -t sh1     # tab 1 'sh1'
screen -S pxeserver -X screen -t sh2     # tab 2 'sh2'
screen -S pxeserver -X screen -t mnt     # tab 3 'mnt'
screen -S pxeserver -X screen -t isohttp # tab 4 'isohttp'
screen -S pxeserver -X screen -t ks      # tab 5 'ks'
screen -S pxeserver -X screen -t vnc     # tab 6 'vnc'

# tab 0 reserved for -e (ethernet) option
# tab 1 reserved for -f (firewall) option

# tab 2, start dnsmasq dhcp/dns/tftp server
screen -S pxeserver -p 2 -X stuff "sudo systemctl start dnsmasq^M"
# Follow journalctl log in new tab 'jctl'
screen -S pxeserver -X screen -t jctl sudo journalctl -f
# Follow kernel ring buffer in new tab 'dmesg'
screen -S pxeserver -X screen -t dmesg dmesg -wH


cleanup()
{
  # terminate the GNU Screen env and run cleanup scripts
  screen -X -S pxeserver quit
  sudo bash "$HOME"/bin/pxe_cleanup.sh
  exit 1
}


# Option flags for screen_pxe.sh
while getopts ":e:f:i:k:v" flag; do
  case "${flag}" in
    e) # In tab 0, put up wired iface and bind to IP addr
       # add argument in the form 'ifacename@ipaddr/subnet' w/ '@' as delimiter
       IFACE=$(echo "$OPTARG" | cut -d "@" -f 1)
       IPADDR=$(echo "$OPTARG" | cut -d "@" -f 2)
       screen -S pxeserver -p 0 -X stuff "sudo bash $HOME/bin/setup_ether.sh $IFACE $IPADDR^M"
       ;;
    f) # In tab 1 run firewall script to open required ports for PXE server
       if [ "$OPTARG" = "firewalld" ]; then
         screen -S pxeserver -p 1 -X stuff "sudo bash $HOME/bin/firewalld_pxe.sh open^M"
       elif [ "$OPTARG" = "ufw" ]; then
         screen -S pxeserver -p 1 -X stuff "sudo bash $HOME/bin/ufw_pxe.sh open^M"
       else
         echo "Enter one of 'firewalld' or 'ufw'!"
         cleanup
       fi
       ;;
    i) # In tab 3, mount the installation ISO image
       if [ -d /mnt/distroIso ]; then
         screen -S pxeserver -p 3 -X stuff "sudo mount $OPTARG /mnt/distroIso^M"
         # In tab 4, serve ISO image over TCP 8080 (darkhttpd default)
         screen -S pxeserver -p 4 -X stuff "darkhttpd /mnt/distroIso^M"
       else
         echo "The mount point /mnt/distroIso does not exist!"
         cleanup
       fi
       ;;
    k) # Ensure that the kickstart parameter is a dir, not a file
       if ! [ -d "$OPTARG" ]; then
         echo "Specify a directory, not a file!" >&2
         cleanup
       else
         # In tab 5, serve kickstart dir over TCP 8000 using darkhttpd
         screen -S pxeserver -p 5 -X stuff "darkhttpd $OPTARG --port 8000^M"
       fi
       ;;
    v) # In tab 6, launch vncviewer in listen mode on TCP 5500
       screen -S pxeserver -p 6 -X stuff "vncviewer --listen^M" ;;
    \?) echo "Invalid option: $OPTARG" >&2
        cleanup ;;
    :) echo "Option -$OPTARG requires an argument." >&2
       cleanup ;;
  esac
done
